const { src, dest, watch, parallel, series } = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const minifyjs = require('gulp-js-minify');
const imagemin = require('gulp-imagemin');

function styles() {
    return src('src/scss/**/*.scss')
        .pipe(autoprefixer())
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS())
        .pipe(scss({outputStyle: 'compressed'}))
        .pipe(dest('src/css'))
        .pipe(browserSync.stream());
};

function scripts() {
    return src('src/js/app.js') 
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(minifyjs())
        .pipe(dest('src/js')) 
        .pipe(browserSync.stream());
};

function watching() {
    watch(['src/scss/**/*.scss'], styles)
    watch(['src/js/app.js'], scripts)
    watch('./src/**/*.html',).on('change', browserSync.reload);
};

function reloadPage() {
    browserSync.init({
        server: {
            baseDir: 'src/',
            port: 3000,
            keeplive: true
        }
    })
};

function building() {
    return src([
        'src/css/style.min.css',
        'src/js/app.min.js',
        'src/*.html'
        ], { base: 'src' })
        .pipe(dest('dist'))
};

function cleanDist() {
    return src('dist/')
        .pipe(clean())
};

function img() {
    return src('src/img/*')
        .pipe(imagemin())
		.pipe(dest('dist/img'))
};

exports.styles = styles;
exports.scripts = scripts;
exports.watching = watching;
exports.reloadPage = reloadPage;

exports.build = series(cleanDist, building, parallel(styles, scripts, img));
exports.dev = parallel(reloadPage, watching)
exports.default = series(this.build, parallel(reloadPage, watching)); 